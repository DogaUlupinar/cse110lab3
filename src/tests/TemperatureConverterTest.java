package tests;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import calculators.CalculatorException;
import calculators.TemperatureConverter;

public class TemperatureConverterTest {

	@Test
	public void testintialconverter() 
	{
		// test for the first requirement
		TemperatureConverter testtempconv = new TemperatureConverter();
		try
		{

			// make sure that temperature tester defaults to celsius to farheneit 
			double test = testtempconv.convert(32);
			assertEquals(89.6,test,1.0);
			
		}
		catch(CalculatorException E)
		{
			fail("should not");
		}
				
	}
	@Test
	public void testExceptionHandling()
	{
	// this is the test for the second requirement
	// test the limits of setCalculator
		double temp1=0; //intial converions wil be used to check if object is preserved
		boolean caughtexception = true;
		TemperatureConverter testtempconv = new TemperatureConverter();
		//test normal value & set of initial object to be preserved
		try
		{
			testtempconv.setCalculator(2);
		}catch(CalculatorException e)
		{
			fail();
		}
		try{temp1 = testtempconv.convert(10);}//intialize
		catch(CalculatorException E){ fail();}
		// test the upper bound
		try
		{
			testtempconv.setCalculator(10);
			caughtexception = false;
		}catch(CalculatorException e)
		{
			System.out.println("success");
		}
		if (!caughtexception)
		{
			fail();
		}
		// test the lower bound
		try
		{
			testtempconv.setCalculator(-1);
			caughtexception = false;
		}catch(CalculatorException e)
		{
			System.out.println("success");
		}
		if (!caughtexception)
		{
			fail();
		}
		//test if the object is preserved
		try
		{
			double temp2 = testtempconv.convert(10);
			// there was an error here, the object was not preserved
			assertEquals(temp1,temp2,1);
		}catch(CalculatorException e)
		{
			fail();
		}
		

	}
	@Test
	public void testmodechange()
	{ // the third requirement also test the 4th requiremnt, both work
		TemperatureConverter testtempconv = new TemperatureConverter();
		boolean exception_caught = false;
		for (int mode = -100; mode < 100; mode++)
		{
			try
			{
				testtempconv.setCalculator(mode);
				double idk = testtempconv.convert(100);
				System.out.print("100 converted is "+idk +"\n");
			}
			catch(CalculatorException E)
			{
				exception_caught = true;
			}
			if (mode > 7 && mode < 0 && !exception_caught)
			{
				fail();
			}
		}
	}
	@Test
	public void testreqfive()
	{
		TemperatureConverter testtc = new TemperatureConverter();
		boolean testpassed = true;
		try
		{
			testtc.convert("convert 10 fahrenheit to kelvin");
		}
		catch(CalculatorException E)
		{
			fail();
		}
		try
		{
			testtc.convert("convert 10 fahrnheit kelvin");
			testpassed = false;
		}
		catch(CalculatorException E)
		{
		// how do you make it do nothing?	
		}
		
	}

}
