package calculators;

import java.util.LinkedList;
import java.util.List;


public class TemperatureConverter {

	public static final int CELSIUS_TO_FAHRENHEIT = 0;
	public static final int CELSIUS_TO_KELVIN = 1;
	public static final int FAHRENHEIT_TO_CELSIUS = 2;
	public static final int FAHRENHEIT_TO_KELVIN = 3;
	public static final int KELVIN_TO_CELSIUS = 4;
	public static final int KELVIN_TO_FAHRENHEIT = 5;
	public static final int NO_CONVERSION = 6;
	
	
	private int calculationMode = CELSIUS_TO_FAHRENHEIT;

	public TemperatureConverter() {
		super();
	}

	public double convert(double T) throws CalculatorException {
		switch (calculationMode) {
		case CELSIUS_TO_FAHRENHEIT:
			return (T * 9.0/5.0) + 32;
		case CELSIUS_TO_KELVIN:
			return T + 273.15;
		case FAHRENHEIT_TO_CELSIUS:
			return (T - 32) * 5.0/9.0;
		case FAHRENHEIT_TO_KELVIN:
			return (T - 32) * 5.0/9.0 + 273.15;
		case KELVIN_TO_CELSIUS:
			return T - 273.15;
		case KELVIN_TO_FAHRENHEIT:
			return (T - 273.15) * 9.0/5.0 + 32;
		case NO_CONVERSION:
			return T;
		default:
			// Throw an exception if the calculation type is unknown
			throw new CalculatorException("Wrong calculation mode set. calculationMode set to " + calculationMode + ".");
		}
	}
	
	public void setCalculator(int mode) throws CalculatorException {
		calculationMode=mode;
		if (mode!=CELSIUS_TO_FAHRENHEIT &&
			mode!=CELSIUS_TO_KELVIN &&
			mode!=FAHRENHEIT_TO_CELSIUS &&
			mode!=FAHRENHEIT_TO_KELVIN &&
			mode!=KELVIN_TO_CELSIUS &&
			mode!=KELVIN_TO_FAHRENHEIT &&
			mode!=NO_CONVERSION)			
		    throw new CalculatorException("Error: trying to set an unsupported mode.");
	}
	
	private List<String> ParseCommand(String s) throws CalculatorException {
		int pos = s.indexOf(' ');
		LinkedList<String> ret = new LinkedList<String>();
		if (pos>=0) {
			String token = s.substring(0, pos); 
			String remaining = s.substring(pos+1);
			ret = new LinkedList<String>(ParseCommand(remaining));
			if (!token.equals(" "))
				ret.addFirst(token);
		} else {
			ret.addFirst(s);
		}
		return ret;
	}
	
	public double convert(String req) throws CalculatorException {
		String[] commands = ParseCommand(req).toArray(new String[0]);
		try {
		if (commands[0].equalsIgnoreCase("convert") &&	commands[3].equalsIgnoreCase("to")) {
			setCalculator(NO_CONVERSION);
			if (commands[2].equalsIgnoreCase("celsius")) {
				if (commands[4].equalsIgnoreCase("kelvin")) {
					setCalculator(CELSIUS_TO_KELVIN);
				} else if (commands[4].equalsIgnoreCase("fahrenheit")) {
					setCalculator(CELSIUS_TO_FAHRENHEIT);
				} 
			} else if (commands[2].equalsIgnoreCase("fahrenheit")) {
				if (commands[4].equalsIgnoreCase("celsius")) {
					setCalculator(CELSIUS_TO_KELVIN);
				} else if (commands[4].equalsIgnoreCase("kelvin")) {
					setCalculator(FAHRENHEIT_TO_KELVIN);
				} 				
			} else if (commands[2].equalsIgnoreCase("kelvin")) {
				if (commands[4].equalsIgnoreCase("celsius")) {
					setCalculator(KELVIN_TO_CELSIUS);
				} else if (commands[4].equalsIgnoreCase("fahrenheit")) {
					setCalculator(KELVIN_TO_FAHRENHEIT);
				} 				
			}
			return convert(Double.parseDouble(commands[1]));
		}
		} catch (Exception e) {
			throw new CalculatorException("Error: impossible to parse the command string - " + req);
		}
		return Double.NaN;
	}

}
